# Maintainer: Daniel Bermond <dbermond@archlinux.org>

# ArtixARM: Platon Ryzhikov <ihummer63@yandex.ru>
# - restrict parallelism
# - build with clang

pkgname=highway
pkgver=1.2.0
pkgrel=1
pkgdesc='A C++ library that provides portable SIMD/vector intrinsics'
arch=('x86_64')
url='https://github.com/google/highway/'
license=('Apache-2.0' 'BSD-3-Clause')
depends=('gcc-libs')
makedepends=('clang' 'cmake' 'gtest')
source=("https://github.com/google/highway/archive/${pkgver}/${pkgname}-${pkgver}.tar.gz")
sha256sums=('7e0be78b8318e8bdbf6fa545d2ecb4c90f947df03f7aadc42c1967f019e63343')

build() {
    # assume the optimal number of cpus is RAM/4
    NPROC=$(($(cat /proc/meminfo | grep MemTotal | awk '{print $2}')/(1024*1024*4)+1))

    cmake -B build -S "${pkgname}-${pkgver}" \
        -G 'Unix Makefiles' \
        -DCMAKE_BUILD_TYPE:STRING='None' \
        -DCMAKE_INSTALL_PREFIX:PATH='/usr' \
        -DBUILD_SHARED_LIBS:BOOL='ON' \
        -DHWY_SYSTEM_GTEST:BOOL='ON' \
        -Wno-dev \
        -DCMAKE_C_COMPILER="/usr/bin/clang" \
        -DCMAKE_CXX_COMPILER="/usr/bin/clang++"
    cmake --build build -j$NPROC
}

check() {
    ctest --test-dir build --output-on-failure
}

package() {
    DESTDIR="$pkgdir" cmake --install build
    install -D -m644 "${pkgname}-${pkgver}/LICENSE-BSD3" -t "${pkgdir}/usr/share/licenses/${pkgname}"
}
