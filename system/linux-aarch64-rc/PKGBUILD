# AArch64 multi-platform
# Maintainer: Kevin Mihelich <kevin@archlinuxarm.org>

# ArtixARM: Platon Ryzhikov <ihummer63@yandex.ru>
# change CONFIG_LOCALVERSION to ARTIX
# add patch to improve Firefly RK3399 performance
# enable HDMI sound on RK3399 boards
# undo commit 96ff264bccb22175bbe2185a1eb5204ca3c5f03f - breaks brcmfmac work
# apply Manjaro ARM patch to speed up boot on RK3399 boards
# mark kernel files with rc to avoid kernels conflict; this should be reflected in 90-linux.hook,
#     linux.preset, *.install

buildarch=8

_rcver=6.10
_rcrel=7

pkgbase=linux-aarch64-rc
_srcname=linux-${_rcver}-rc${_rcrel}
_kernelname=${pkgbase#linux}
_desc="AArch64 multi-platform (release candidate)"
pkgver=${_rcver}.rc${_rcrel}
pkgrel=1
arch=('aarch64')
url="http://www.kernel.org/"
license=('GPL2')
makedepends=('xmlto' 'docbook-xsl' 'kmod' 'inetutils' 'bc' 'git' 'uboot-tools' 'vboot-utils' 'dtc' 'python-sphinx')
options=('!strip')
source=("https://git.kernel.org/torvalds/t/${_srcname}.tar.gz"
        '0001-net-smsc95xx-Allow-mac-address-to-be-set-as-a-parame.patch'
        '0002-arm64-dts-rockchip-disable-pwm0-on-rk3399-firefly.patch'
        '0001-firefly-rk3399.patch'
        '0002-rk3399-enable-hdmi-sound.patch'
        '0003-undo-brcm.patch'
        '0001-arm64-dts-rockchip-remove-capacity-dmips-rk3399.patch'
        'config'
        'generate_chromebook_its.sh'
        'kernel.keyblock'
        'kernel_data_key.vbprivk'
        'linux.preset'
        '60-linux.hook'
        '90-linux.hook')
md5sums=('0a032f54ae850dd98efd20f729626bd5'
         '37309f1d508eef099622100d9810bb56'
         '369fb1dd626c4e0a4a7e068d27dd51c8'
         '9d61cbe771c266a7127c6d800cb49538'
         'f4b641a6792c05380598de0f3cb64863'
         'f6387c523bb62760152c55935df406bb'
         '481f71a08407ec05b2cc055067290015'
         '709ecabbf36c18d8226dea100824485e'
         'cb97c1ab32e0165cde8fb5eb68302225'
         '61c5ff73c136ed07a7aadbf58db3d96a'
         '584777ae88bce2c5659960151b64c7d8'
         'a0933a60d3b5a8368f4cd697f10e7d39'
         'ce6c81ad1ad1f8b333fd6077d47abdaf'
         '20025c15dbd10b7e4b6a0707e1716e4e')

prepare() {
  cd "${srcdir}/${_srcname}"

  # ALARM patches
  git apply ../0001-net-smsc95xx-Allow-mac-address-to-be-set-as-a-parame.patch
  git apply ../0002-arm64-dts-rockchip-disable-pwm0-on-rk3399-firefly.patch
  # ArtixARM pathces
  git apply ../0001-firefly-rk3399.patch
  git apply ../0002-rk3399-enable-hdmi-sound.patch
  git apply ../0003-undo-brcm.patch
  # Manjaro ARM patches
  git apply ../0001-arm64-dts-rockchip-remove-capacity-dmips-rk3399.patch

  cat "${srcdir}/config" > ./.config

  # add pkgrel to extraversion
  sed -ri "s|^(EXTRAVERSION =)(.*)|\1 \2-${pkgrel}|" Makefile

  # don't run depmod on 'make install'. We'll do this ourselves in packaging
  sed -i '2iexit 0' scripts/depmod.sh
}

build() {
  cd "${srcdir}/${_srcname}"

  # get kernel version
  make prepare

  # load configuration
  # Configure the kernel. Replace the line below with one of your choice.
  #make menuconfig # CLI menu for configuration
  #make nconfig # new CLI menu for configuration
  #make xconfig # X-based configuration
  #make oldconfig # using old config from previous kernel version
  # ... or manually edit .config

  # Copy back our configuration (use with new kernel version)
  #cp ./.config ../${pkgbase}.config

  ####################
  # stop here
  # this is useful to configure the kernel
  #msg "Stopping build"
  #return 1
  ####################

  #yes "" | make config

  # build!
  unset LDFLAGS
  make ${MAKEFLAGS} Image Image.gz modules
  # Generate device tree blobs with symbols to support applying device tree overlays in U-Boot
  make ${MAKEFLAGS} DTC_FLAGS="-@" dtbs
}

_package() {
  pkgdesc="The Linux Kernel and modules - ${_desc}"
  depends=('coreutils' 'linux-firmware' 'kmod' 'mkinitcpio>=0.7')
  optdepends=('crda: to set the correct wireless channels of your country')
  provides=("linux=${pkgver}" "WIREGUARD-MODULE")
  replaces=('linux-armv8-rc')
  backup=("etc/mkinitcpio.d/${pkgbase}.preset")
  install=${pkgname}.install

  cd ${_srcname}

  KARCH=arm64

  # get kernel version
  _kernver="$(make kernelrelease)"
  _basekernel=${_kernver%%-*}
  _basekernel=${_basekernel%.*}

  mkdir -p "${pkgdir}"/{boot,usr/lib/modules}
  make INSTALL_MOD_PATH="${pkgdir}/usr" modules_install
  make INSTALL_DTBS_PATH="${pkgdir}/boot/dtbs-rc" dtbs_install
  cp arch/$KARCH/boot/Image "${pkgdir}/boot/rcImage"
  cp arch/$KARCH/boot/Image.gz "${pkgdir}/boot/rcImage.gz"

  # make room for external modules
  local _extramodules="extramodules-${_basekernel}${_kernelname}"
  ln -s "../${_extramodules}" "${pkgdir}/usr/lib/modules/${_kernver}/extramodules"

  # add real version for building modules and running depmod from hook
  echo "${_kernver}" |
    install -Dm644 /dev/stdin "${pkgdir}/usr/lib/modules/${_extramodules}/version"

  # remove build and source links
  rm "${pkgdir}"/usr/lib/modules/${_kernver}/{source,build} || true

  # now we call depmod...
  depmod -b "${pkgdir}/usr" -F System.map "${_kernver}"

  # sed expression for following substitutions
  local _subst="
    s|%PKGBASE%|${pkgbase}|g
    s|%KERNVER%|${_kernver}|g
    s|%EXTRAMODULES%|${_extramodules}|g
  "

  # install mkinitcpio preset file
  sed "${_subst}" ../linux.preset |
    install -Dm644 /dev/stdin "${pkgdir}/etc/mkinitcpio.d/${pkgbase}.preset"

  # install pacman hooks
  sed "${_subst}" ../60-linux.hook |
    install -Dm644 /dev/stdin "${pkgdir}/usr/share/libalpm/hooks/60-${pkgbase}.hook"
  sed "${_subst}" ../90-linux.hook |
    install -Dm644 /dev/stdin "${pkgdir}/usr/share/libalpm/hooks/90-${pkgbase}.hook"
}

_package-headers() {
  pkgdesc="Header files and scripts for building modules for linux kernel - ${_desc}"
  provides=("linux-headers=${pkgver}")
  replaces=('linux-armv8-rc-headers')

  cd ${_srcname}
  local builddir="${pkgdir}/usr/lib/modules/${_kernver}/build"

  echo "Installing build files..."
  install -Dt "$builddir" -m644 .config Makefile Module.symvers System.map vmlinux
  install -Dt "$builddir/kernel" -m644 kernel/Makefile
  install -Dt "$builddir/arch/${KARCH}" -m644 arch/${KARCH}/Makefile
  cp -t "$builddir" -a scripts

  mkdir -p "$builddir"/{fs/xfs,mm}

  echo "Installing headers..."
  cp -t "$builddir" -a include
  cp -t "$builddir/arch/${KARCH}" -a arch/${KARCH}/include
  install -Dt "$builddir/arch/${KARCH}/kernel" -m644 arch/${KARCH}/kernel/asm-offsets.s
  mkdir -p "$builddir/arch/arm"
  cp -t "$builddir/arch/arm" -a arch/arm/include

  install -Dt "$builddir/drivers/md" -m644 drivers/md/*.h
  install -Dt "$builddir/net/mac80211" -m644 net/mac80211/*.h

  # https://bugs.archlinux.org/task/13146
  install -Dt "$builddir/drivers/media/i2c" -m644 drivers/media/i2c/msp3400-driver.h

  # https://bugs.archlinux.org/task/20402
  install -Dt "$builddir/drivers/media/usb/dvb-usb" -m644 drivers/media/usb/dvb-usb/*.h
  install -Dt "$builddir/drivers/media/dvb-frontends" -m644 drivers/media/dvb-frontends/*.h
  install -Dt "$builddir/drivers/media/tuners" -m644 drivers/media/tuners/*.h

  # https://bugs.archlinux.org/task/71392
  install -Dt "$builddir/drivers/iio/common/hid-sensors" -m644 drivers/iio/common/hid-sensors/*.h

  echo "Installing KConfig files..."
  find . -name 'Kconfig*' -exec install -Dm644 {} "$builddir/{}" \;

  echo "Removing unneeded architectures..."
  local arch
  for arch in "$builddir"/arch/*/; do
    [[ $arch = */${KARCH}/ || $arch == */arm/ ]] && continue
    echo "Removing $(basename "$arch")"
    rm -r "$arch"
  done

  echo "Removing documentation..."
  rm -r "$builddir/Documentation"

  echo "Removing broken symlinks..."
  find -L "$builddir" -type l -printf 'Removing %P\n' -delete

  echo "Removing loose objects..."
  find "$builddir" -type f -name '*.o' -printf 'Removing %P\n' -delete

  echo "Stripping build tools..."
  local file
  while read -rd '' file; do
    case "$(file -bi "$file")" in
      application/x-sharedlib\;*)      # Libraries (.so)
        strip -v $STRIP_SHARED "$file" ;;
      application/x-archive\;*)        # Libraries (.a)
        strip -v $STRIP_STATIC "$file" ;;
      application/x-executable\;*)     # Binaries
        strip -v $STRIP_BINARIES "$file" ;;
      application/x-pie-executable\;*) # Relocatable binaries
        strip -v $STRIP_SHARED "$file" ;;
    esac
  done < <(find "$builddir" -type f -perm -u+x ! -name vmlinux -print0)

  echo "Adding symlink..."
  mkdir -p "$pkgdir/usr/src"
  ln -sr "$builddir" "$pkgdir/usr/src/$pkgbase"
}

_package-chromebook() {
  pkgdesc="The Linux Kernel - ${_desc} - Chromebooks"
  depends=('linux-aarch64-rc')
  install=${pkgname}.install

  cd "${srcdir}/${_srcname}"

  mkdir -p "${pkgdir}/boot"

  KARCH=arm64
  image=arch/${KARCH}/boot/Image

  chromeos_boards=(
    'elm'
    'gru'
    'kukui'
    'trogdor'
    'asurada'
  )
  chromebook_dtbs=($(for b in ${chromeos_boards[@]}; do find arch/${KARCH}/boot -name "*${b}*.dtb" | LC_COLLATE=C sort; done))

  for to_compress in ${chromebook_dtbs[@]} ${image}; do
    #lzma -9 -z -f -k ${to_compress} # This is 40% smaller but takes ~1 sec longer to boot
    lz4 -20 -z -f ${to_compress} ${to_compress}.lz4
  done
  echo ${chromebook_dtbs[@]/%/.lz4} | ../generate_chromebook_its.sh ${image}.lz4 ${KARCH} lz4 > kernel.its

  mkimage -D "-I dts -O dtb -p 2048" -f kernel.its vmlinux-rc.uimg
  dd if=/dev/zero of=bootloader.bin bs=512 count=1
  echo "console=tty0 console=ttyS2,115200n8 earlyprintk=ttyS2,115200n8 init=/sbin/init root=PARTUUID=%U/PARTNROFF=1 rootwait rw noinitrd" > cmdline
  vbutil_kernel \
    --pack vmlinux-rc.kpart \
    --version 1 \
    --vmlinuz vmlinux-rc.uimg \
    --arch aarch64 \
    --keyblock ../kernel.keyblock \
    --signprivate ../kernel_data_key.vbprivk \
    --config cmdline \
    --bootloader bootloader.bin

  cp vmlinux-rc.kpart "${pkgdir}/boot"
}

pkgname=("${pkgbase}" "${pkgbase}-headers" "${pkgbase}-chromebook")
for _p in ${pkgname[@]}; do
  eval "package_${_p}() {
    _package${_p#${pkgbase}}
  }"
done
