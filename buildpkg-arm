#!/bin/sh
#TODO: commit, push, pull
ARCH='aarch64'
BUILDDIR="./build"
REPOSDIR="./packages"
SOURCESDIR="."
CHROOT="."

addrepo=
build=1
mode='arm'
repotype='stable'
trunk=
commit=
overwrite=1
push=
clean=1
arguments=""
mkchrootpkgargs=""
#distcc="distcc-3.3.2-1-aarch64.pkg.tar.xz"

usage() {
	echo "Usage: ${0##*/} [options]"
	echo '    -p <pkg>      Package name'
	echo '    -o "args"     Extra arguments for makepkg'
	echo '    -b "args"     Extra arguments for mkchrootpkg'
	echo '    -d "package"  distcc package'
	echo '    -r <chroot>   See mkchrootpkg'
	echo '    -e            Add to repo'
	echo "    -n            Don't build"
	echo "    -w            Use existimg file in builddir"
	echo "    -l            Don't remove files from builddir"
	echo "    -a            Work with artix repos"
	echo "    -t            Use testing version"
	echo "    -s            Use staging version"
	echo "    -c            Commit changes"
	echo "    -u            Push changes"
	echo '    -h            This help'
	echo ''
	echo ''
	exit $1
}

orig_argv=("$0" "$@")

opts='p:o:r:b:d:enwlatscuh'

while getopts "${opts}" arg; do
	case "${arg}" in
		p) package="$OPTARG" ;;
		d) distcc="$OPTARG" ;;
		r) CHROOT="$OPTARG" ;;
		e) addrepo=1 ;;
		n) build= ;;
		w) overwrite= ;;
		l) clean= ;;
		a) mode='artix' ;;
		t) repotype='testing' ;;
		s) repotype='staging' ;;
		c) commit=1 ;;
		u) push=1 ;;
		o) arguments="$OPTARG" ;;
		b) mkchrootpkgargs="$OPTARG" ;;
		h|?) usage 0 ;;
		*) echo "invalid argument '${arg}'"; usage 1 ;;
	esac
done

arguments+="--ignorearch"

if [[ ! $package && ! $push ]]; then
	echo "wrong action"; usage 1;
fi

if [[ $mode == 'arm' ]]; then
	pkg=$(find $SOURCESDIR/artixarm -maxdepth 2 -name $package)
	repo=$(echo $pkg | awk -F '/' '{print $3}')
	if [[ ! $pkg ]]; then mode='artix'
	else
		if [[ $build ]]; then
			[ -d $CHROOT/root ] || { echo "chroot is not valid!"
				exit 1
			}
			echo "Building package $package"
			if [[ $overwrite ]]; then
				cp -r $pkg $BUILDDIR
			fi
			cd $BUILDDIR/$package
			mkchrootpkg $mkchrootpkgargs -r $CHROOT -- $arguments || {
				cd ../..
				exit 1
			}
			cd ../..
		fi
	fi
fi

if [[ $mode == 'artix' ]]; then
	pkg=$(find $SOURCESDIR/artix -maxdepth 2 -name $package)/repos
	repo=$(ls $pkg | sed -n '1p' | awk -F '-' '{print $1}')
	origrepo="$repo"
	if [[ $repotype == 'testing' || $repotype == 'staging' ]]; then
		if [[ $repo == 'extra' || $repo == 'core' ]]; then
			origrepo="$repotype"
		else origrepo="community-$repotype"
		fi
	fi
		
	case $repo in
		core)
			repo='system'
		;;
		extra)
			repo='world'
		;;
		community)
			repo='galaxy'
		;;
	esac
	
	if [[ $build && $package ]]; then
		[ -d $CHROOT/root ] || { echo "chroot is not valid!"
			exit 1
		}
		echo "Building package $package"
		if [[ $overwrite ]]; then
			mkdir $BUILDDIR/$package
			cp -r $pkg/$(ls $pkg | grep $origrepo | sed -n '1p')/* $BUILDDIR/$package
		fi
		cd $BUILDDIR/$package
		mkchrootpkg $mkchrootpkgargs -r $CHROOT -- $arguments || {
			cd ../..
			exit 1
		}
		cd ../..
	fi
fi

if [[ $addrepo && $package ]]; then
	list=$(find $BUILDDIR/$package -name "*.pkg.tar.xz")
	echo "Adding $package to repos database"
	for item in $list; do
		item=$(basename $item)
		echo "Checking for old entries for $item"
		for remove in $(find $REPOSDIR/$repo/os/$ARCH -name "$(echo $item | sed -e 's/\b-[0-9].*\b//g')-[0-9]*"); do
			echo "Removing $remove"
			rm $remove
		done	
		echo "Adding $item"
		cp $BUILDDIR/$package/$item $REPOSDIR/$repo/os/$ARCH
		cp $BUILDDIR/$package/$item.sig $REPOSDIR/$repo/os/$ARCH
		repo-add -R $REPOSDIR/$repo/os/$ARCH/$repo.db.tar.xz $REPOSDIR/$repo/os/$ARCH/$item
	done
fi

if [[ $clean && $package ]]; then
	echo "Cleaning $package"
	rm -rf $BUILDDIR/$package
fi
if [[ $commit ]]; then
	echo
fi
if [[ $push ]]; then
	echo
fi
